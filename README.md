``# Azure Databrick Workshop


## Description

Procedimiento realizado durante el workshop para levantamiento de un cluster temporal como usuario community

### Requerimientos:

    * conexion a internet, cuenta de correo

### **Creacion de una cuenta ```Databricks```**
1: Ingresar a la direccion [databricks](https://databricks.com/)

2: Hacer click sobre 
**```Try IT FOR FREE```**

3: Llenar formulario
  * Title: Engineer
  * Company: Nodel
  * Phone: [Vacio]


4: Hacer click sobre **```GET STARTED```** abajo de COMMUNITY EDITION 


5: Esperar un correo de bienvenida: Welcome to Databricks Community Edition!
y hacer click en link adjunto

6: Se abrira un formulario para setear su clave de acceso

7: Listo! 


### **Creacion de un Cluster e inicializacion de data para el taller**

1: Buscar el boton 'Compute' en el menu deplegable de la izquierda

2: Nombrar el cluster como cluster_taller o similar

3: Explorar las opciones de Runtime disponibles y seleccionar -> 8.2 (includes Apache Spark 3.1.1, Scala 2.12)

4: Esperar el inicio del cluster

5: Una vez inicializado el cluster buscar el icono de 'data' en el menu desplegable y hacer click sobre create table

6: Seleccionar la opcion Upload file en el menu de data source dentro de la pagina create new table 

7: crear el path   `FileStore/tables/retail_db/orders/` y arrastrar el archivo `part_00000` presente en este repositorio

8: Presionar sobre create table with UI y seleccionar el cluster creado anteriormente

9: verificar la data presionando preview table

10: Seleccionar la opcion infer schema y finalmente presionar Create table

### **Creacion de Notebook y Carga de data en formato spark**

1: Buscar en el menu desplegable la opcion create Notebook

2: En la ventana resultante nombrar el notebook taller, seleccionar python como base y el cluster actual

3: Dentro de la primera celda ejecutar el commando sql:
  `%sql describe detail default.part_00000`

4: Agregar una celda diferente y cargar la data mediante:

   `data=spark.sql('select * from default.part_00000')`
   
   `data.show()`
