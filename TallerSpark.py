# Databricks notebook source
# MAGIC %sql select * from default.part_00000

# COMMAND ----------

# MAGIC %sql DESCRIBE DETAIL default.part_00000

# COMMAND ----------

data = spark.sql('select * from default.part_00000')
data = data.toDF('id','date','value','status')
display(data)

# COMMAND ----------

#data.write.format("delta").mode("overwrite").saveAsTable("default.orders_test")
#data.write.format("delta").mode("overwrite").option("overwriteSchema", "true").saveAsTable("default.orders_test")

# COMMAND ----------

#%sql describe detail default.orders_test

# COMMAND ----------

# Using SQL with .select()
data.filter("value<500").select(['value','status']).show()

# COMMAND ----------

data.filter( (data["value"] < 200) & (data['status'] == 'COMPLETE') ).show()

# COMMAND ----------

data=data.withColumn('new_value',data['value']*2)#.show()

# COMMAND ----------

display(data)

# COMMAND ----------

data.createOrReplaceTempView("orders_view")

# COMMAND ----------

result = spark.sql('select * from orders_view')
display(result)

# COMMAND ----------

data.orderBy(data["value"].desc()).show()

# COMMAND ----------

data.groupBy("new_value").mean('value').show()

# COMMAND ----------

data.groupBy("status").agg({'value':'avg'}).show()

# COMMAND ----------

data.groupBy("status").count().show()

# COMMAND ----------

from pyspark.sql.functions import countDistinct, avg,stddev
data.select(countDistinct("status").alias("Distinct Status")).show()

# COMMAND ----------

data.select(avg("value").alias("Avg value")).show()

# COMMAND ----------

#PARQUET
#DELTA

# COMMAND ----------

data.write.format("delta").mode("overwrite").saveAsTable("default.orders")
data.write.format("parquet").mode("overwrite").saveAsTable("default.orders2")
#data.write.format("delta").mode("overwrite").option("overwriteSchema", "true").saveAsTable("default.orders_test")

# COMMAND ----------

# MAGIC %sql describe detail default.orders

# COMMAND ----------

# MAGIC %sql delete from default.orders2 where id=1

# COMMAND ----------

# MAGIC %sql delete from default.orders where id=1

# COMMAND ----------

data.write.format("delta").mode("append").saveAsTable("default.orders3")
data.write.format("delta").mode("append").saveAsTable("default.orders3")

# COMMAND ----------

# MAGIC %sql select count(*) from default.orders3

# COMMAND ----------

data2 = data.drop("new_value")

# COMMAND ----------

data2.write.format("delta").mode("overwrite").option("overwriteSchema", "true").saveAsTable("default.orders3")

# COMMAND ----------

# MAGIC %sql select * from default.orders3

# COMMAND ----------

#%sql INSERT INTO TABLE default.orders3 VALUES ('50000','2020-06-25 00:00:00.0','1000','COMPLETE'),('50001','2020-06-25 00:00:00.0','1000','COMPLETE')
